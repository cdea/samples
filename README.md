Instructions
-----------------
To execute any of the samples, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`
* Desktop
> `./gradlew run`

Samples in this directory
---------------------------------
Comments
========

 A JavaFX Application that uses Gluon Charm: 

 - Charm Connect: the client-side counterpart of the Gluon Cloud service 
 - Charm Glisten: Simple application framework, custom UI controls and customized existing JavaFX UI controls

Comments 2.0
============

The enhanced version of the Comments sample with new features to show more of the potential of Gluon Charm.

 - User authentication
 - Avatar control
 - SlidingListTile


Notes
=====

A JavaFX application using Gluon Charm:

 - Charm Glisten: to make the UI beautiful without boring UI guidelines
 - Charm Connect: for locally storing Notes and Settings

50 States
=========

A JavaFX Application that uses a new Gluon Charm custom control, CharmListView. 

- Charm Glisten: Simple application framework, custom UI controls and customized existing JavaFX UI controls


GoNative
========

This sample shows how to handle native interaction with hardware devices, and it was explained in detail in [this post](http://gluonhq.com/handling-android-native-activities-like-a-pro/) how to add Android native code to the project.

CodeVault
=========

A JavaFX desktop application that uses Gluon Particle:
 
 - Showing the best practices for using Particle.

GluonSQLite
===========

This sample shows how to embed a SQLite database in your project, and deploy it on desktop, Android, iOS and embedded devices, as it was explained in detail in [this post](http://gluonhq.com/embedding-database-cross-platform-projects/).

Beacons
=======

This sample shows how to use the Bluetooth Low Energy service in Charm Down, to scan beacons with your mobile device, as explained in detail in this [post](http://gluonhq.com/scanning-bluetooth-low-energy-beacons-charm/).