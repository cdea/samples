package com.gluonhq.demo.notes;

import com.gluonhq.demo.notes.views.NoteEditorView;
import com.gluonhq.demo.notes.views.NotesListPresenter;
import com.gluonhq.demo.notes.views.NotesListView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.layout.layer.SidePopupView;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.demo.notes.views.UserDrawerView;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;

/** NotesApp is a sample app that uses Gluon Mobile http://gluonhq.com/products/charm/
 * 
 * It also uses Afterburner framework https://github.com/AdamBien/afterburner.fx/
 * 
 * This is the main class of the project, extending MobileApplication, which also extends Application.
 * The developer just need to specify the views and layers in their application, 
 * and provide these as factories that can be called on-demand, starting from the 'home_view', 
 * which is shown when the application first starts.
 *
 */
public class NotesApp extends MobileApplication {

    public static final String NOTE_LIST_VIEW = HOME_VIEW;
    public static final String NOTE_CREATOR_VIEW = "Add a Note";
    public static final String NOTE_EDIT_VIEW = "Edit a Note";
    public static final String NOTE_SETTINGS = "App Settings";
    public static final String USER_DRAWER = "userDrawer";

    private NotesListPresenter notesListPresenter;
    
    private final ChangeListener listener = (obs, ov, nv) -> {
                    hideLayer(USER_DRAWER);
                    switchView(((NavigationDrawer.Item) nv).getTitle());
                };
    
    @Override
    public void init() {

        /*
        Create default home view with list of notes
        */
        
        addViewFactory(HOME_VIEW, () -> {
            NotesListView notesListView = new NotesListView();
            notesListPresenter = (NotesListPresenter) notesListView.getPresenter();
            return (View) notesListView.getView();
        });
        
        /*
        Create other views, to add and edit notes
        */
        
        addViewFactory(NOTE_CREATOR_VIEW, () -> (View) (new NoteEditorView().getView()));
        addViewFactory(NOTE_EDIT_VIEW, () -> (View) (new NoteEditorView().getView()));
        addViewFactory(NOTE_SETTINGS, () -> (View) (new UserDrawerView().getView()));

        /*
        Add side menu with settings
        */
        NavigationDrawer navigationDrawer = new NavigationDrawer();
        NavigationDrawer.Header header = new NavigationDrawer.Header("Notes",
                "A Gluon App",
                new Avatar(21, new Image("/icon.png")));
        navigationDrawer.setHeader(header);
        
        navigationDrawer.getItems().setAll(
            new NavigationDrawer.Item(HOME_VIEW, MaterialDesignIcon.HOME.graphic()),
            new NavigationDrawer.Item(NOTE_CREATOR_VIEW, MaterialDesignIcon.NOTE_ADD.graphic()),
            new NavigationDrawer.Item(NOTE_SETTINGS, MaterialDesignIcon.SETTINGS.graphic()));
        
        navigationDrawer.selectedItemProperty().addListener(listener);
        
        viewProperty().addListener((obs, ov, nv) -> {
            for (Node node : navigationDrawer.getItems()) {
                NavigationDrawer.Item item = (NavigationDrawer.Item) node;
                if (item.getTitle().equals(nv.getName())) {
                    navigationDrawer.selectedItemProperty().removeListener(listener);
                    navigationDrawer.setSelectedItem(node);
                    item.setSelected(true);
                    navigationDrawer.selectedItemProperty().addListener(listener);
                } else {
                    item.setSelected(false);
                }
            }
        });
        
        addLayerFactory(USER_DRAWER, () -> new SidePopupView(navigationDrawer));
    }
    
    /**
     * Called once we have a scene, allowing first run settings 
     * @param scene 
     */
    @Override
    public void postInit(Scene scene) {
        Swatch.LIGHT_GREEN.assignTo(scene);

        scene.getStylesheets().add(NotesApp.class.getResource("style.css").toExternalForm());
        
        // once scene is ready, retrieve notes
        notesListPresenter.postInit();
    }
}
