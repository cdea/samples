package com.gluonhq.demo.notes.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Simple POJO
 */
public class Note {

    private final StringProperty titleProperty = new SimpleStringProperty();
    private final StringProperty textProperty = new SimpleStringProperty();
    private long creationDate;
    
    public Note() {
        creationDate = System.currentTimeMillis();
    }

    Note(String title, String text) {
        this.titleProperty.set(title);
        this.textProperty.set(text);
    }

    /**
     * @return the text
     */
    public String getText() {
        return textProperty.get();
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.textProperty.set(text);
    }
    
    public StringProperty textProperty() {
        return this.textProperty;
    }

    /**
     * @return the creationDate
     */
    public long getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return titleProperty.get();
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.titleProperty.set(title);
    }

    public StringProperty titleProperty () {
        return this.titleProperty;
    }
    
    @Override
    public String toString() {
        return super.toString() + " with title " + titleProperty.get();
    }
}
