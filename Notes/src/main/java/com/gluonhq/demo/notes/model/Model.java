package com.gluonhq.demo.notes.model;

import javax.inject.Singleton;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Singleton class maintaining selected note so it can be edited.
 * Using Afterburner, this is a service that is injected in the Presenter classes
 */
@Singleton
public class Model {
    
    private final ObjectProperty<Note> activeNote = new SimpleObjectProperty<>();
    
    public ObjectProperty<Note> activeNote() {
        return activeNote;
    }
    
}
