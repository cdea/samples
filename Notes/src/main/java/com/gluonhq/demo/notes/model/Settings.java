package com.gluonhq.demo.notes.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Simple set of settings that can be applied to listview
 * and can be modified in the side menu
 */
public class Settings {
    
    /**
     * enum with different sorting options
     */
    public static enum SORTING {
        DATE(0), 
        TITLE(1), 
        CONTENT(2);
        
        final int id;
        
        SORTING(int id) {
            this.id = id;
        }
    
        public int getId() {
            return id;
        }
        
        public static SORTING byId(int id) {
            for (SORTING s : SORTING.values()) {
                if (s.getId() == id) {
                    return s;
                }
            }
            return DATE;
        }
    }
    
    // sortingProperty (enum is not persisted)
    private final ObjectProperty<SORTING> sortingProperty = new SimpleObjectProperty<SORTING>(this, "sorting", SORTING.DATE) {
        @Override
        protected void invalidated() {
            setSortingId(get().getId());
        }
        
    };
    
    public final ObjectProperty<SORTING> sortingProperty() {
       return sortingProperty;
    }
    
    public final SORTING getSorting() {
       return sortingProperty.get();
    }
    
    public final void setSorting(SORTING value) {
        sortingProperty.set(value);
    }
    
    // sortingIdProperty
    private final IntegerProperty sortingIdProperty = new SimpleIntegerProperty(this, "sortingId", SORTING.DATE.getId()) {
        @Override
        protected void invalidated() {
            setSorting(SORTING.byId(get()));
        }

    };
    
    public final IntegerProperty sortingIdProperty() {
       return sortingIdProperty;
    }
    
    public final int getSortingId() {
       return sortingIdProperty.get();
    }
    
    public final void setSortingId(int value) {
        sortingIdProperty.set(value);
    }
    
    // showDateProperty
    private final BooleanProperty ascendingProperty = new SimpleBooleanProperty(this, "ascending", true);
    
    public final BooleanProperty ascendingProperty() {
       return ascendingProperty;
    }
    
    public final boolean isAscending() {
       return ascendingProperty.get();
    }
    
    public final void setAscending(boolean value) {
        ascendingProperty.set(value);
    }

    // fontSizeProperty
    private final IntegerProperty fontSizeProperty = new SimpleIntegerProperty(this, "fontSize", 10);
    
    public final IntegerProperty fontSizeProperty() {
       return fontSizeProperty;
    }
    
    public final int getFontSize() {
       return fontSizeProperty.get();
    }
    
    public final void setFontSize(int value) {
        fontSizeProperty.set(value);
    }
    
    // showDateProperty
    private final BooleanProperty showDateProperty = new SimpleBooleanProperty(this, "showDate", true);
    
    public final BooleanProperty showDateProperty() {
       return showDateProperty;
    }
    
    public final boolean isShowDate() {
       return showDateProperty.get();
    }
    
    public final void setShowDate(boolean value) {
        showDateProperty.set(value);
    }
    
}
