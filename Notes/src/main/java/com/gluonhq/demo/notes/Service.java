package com.gluonhq.demo.notes;

import com.gluonhq.charm.connect.service.*;
import com.gluonhq.demo.notes.model.Settings;
import com.gluonhq.demo.notes.model.Note;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import com.gluonhq.charm.connect.GluonClient;
import com.gluonhq.charm.connect.GluonClientBuilder;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.annotation.PostConstruct;

/** Service to access the application on local storage, 
 * retrieve a list with notes and an object with settings
 * 
 * Using local storage doesn't requiere credentials on Gluon's Portal
 */
public class Service {
    /*
    Every list stored under the same application has a unique id:
    */
    private static final String NOTES = "notes v1.0";
    private static final String NOTES_SETTINGS = "notes-settings";
    /*
    An observable wrapper of the retrieved list of notes, used to expose it and bind the 
    ListView items to the list.
    */
    private final ListProperty<Note> notes = new SimpleListProperty<>(FXCollections.observableArrayList());
    
    /*
    Object to store app settings 
    */
    private final ObjectProperty<Settings> settings = new SimpleObjectProperty<>(new Settings());
    
    private GluonClient gluonClient;
    private StorageService userStorage;

    /**
     * See Afterburner.fx
     */
    @PostConstruct
    public void postConstruct() {
        // no credentials required for local storage
        gluonClient = GluonClientBuilder.create().build();
        userStorage = gluonClient.getStorageService();
    }
    
    // notes

    /**
     * Once there's a valid storageService, the contents of the list can be retrieved. 
     * This will return a CharmObservableList. Note the flags:
     * - LIST_WRITE_THROUGH: Changes in the local list are reflected to the remote copy of that list on Gluon Cloud
     * - WRITE_THROUGH: Changes in the observable properties of objects in the local list are reflected to the remote copy on Gluon Cloud
     *
     * This means that any change done in any client app will be reflected in the local storage
     */
    public void retrieveNotes() {
        CharmObservableList<Note> charmNotes = userStorage.<Note>retrieveList(NOTES,
                Note.class, StorageWhere.DEVICE, SyncFlag.LIST_WRITE_THROUGH, SyncFlag.OBJECT_WRITE_THROUGH);
        charmNotes.stateProperty().addListener((obs, ov, nv) -> {
            if (nv.equals(CharmObservableList.State.INITIALIZED)) {
                notes.set(charmNotes);
                
                // After retrieving the notes, get the settings
                retrieveSettings();
            }
        });
    }
    
    /**
     * Remove note from list
     * @param note 
     */
    
    public void removeNote(Note note) {
        notes.get().remove(note);
    }

    /**  
     * Add a new note to the list
     * @param note
     * @return Note
     */
    public Note addNote(Note note) {
        notes.get().add(note);
        return note;
    }

    /**
     *
     * @return: the wrapper of the remote list of notes
     */
    public ListProperty<Note> notesProperty() {
        return notes;
    }
    
    // settings    
    
    /**
     * Retrieves local object from device, without flags (no sync)
     */
    private void retrieveSettings() {
        CharmObservableObject<Settings> charmSettings = userStorage.<Settings>retrieveObject(NOTES_SETTINGS, 
                Settings.class, StorageWhere.DEVICE);
        charmSettings.stateProperty().addListener((obs, ov, nv) -> {
            if (nv.equals(CharmObservableObject.State.INITIALIZED) && charmSettings.get() != null) {
                settingsProperty().set(charmSettings.get());
            }
        });
    }
    
    /**
     * Persists changes in settings in local storage
     */
    public void storeSettings() {
        userStorage.<Settings>storeObject(NOTES_SETTINGS, settings.get(), StorageWhere.DEVICE);
    }

    /**
     *
     * @return: the wrapper of the remote object of settings.
     */
    public ObjectProperty<Settings> settingsProperty() {
        return settings;
    }

}
