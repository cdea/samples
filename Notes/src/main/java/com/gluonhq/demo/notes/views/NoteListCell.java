package com.gluonhq.demo.notes.views;

import com.gluonhq.demo.notes.model.Note;
import java.util.function.Consumer;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;

import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.notes.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javafx.beans.value.ChangeListener;

/**
 * Custom cell with a ListTile to display the note and some buttons with custom actions
 */
public class NoteListCell extends ListCell<Note> {
    
    private final ListTile tile;
    private final SimpleDateFormat dateFormat;
    private Note currentItem;
    
    private final ChangeListener<String> noteChangeListener;

    private final Service service;
    
    public NoteListCell( Service service, Consumer<Note> removeOp , Consumer<Note> editOp) {
        this.service = service;
        
        tile = new ListTile();
        tile.setPrimaryGraphic(MaterialDesignIcon.DESCRIPTION.graphic());
        
        Button btnEdit = MaterialDesignIcon.EDIT.button(e -> editOp.accept(currentItem));
        Button btnRemove = MaterialDesignIcon.DELETE.button(e -> removeOp.accept(currentItem));
        HBox buttonBar = new HBox(0, btnEdit, btnRemove);
        buttonBar.setAlignment(Pos.CENTER_RIGHT);

        tile.setSecondaryGraphic(buttonBar);
            
        dateFormat = new SimpleDateFormat("EEE, MMM dd yyyy HH:mm", Locale.ENGLISH);
            
        // in case notes change in edit mode, update cell with settings
        noteChangeListener = (obs, ov, nv) -> updateWithSettings();
        
        //update cell if settings change
        service.settingsProperty().addListener((obs, ov, nv) -> updateWithSettings());
        
    }
    
    @Override
    protected void updateItem(Note item, boolean empty) {
        super.updateItem(item, empty);
        
        updateCurrentItem(item);
        if (!empty && item != null) {
            updateWithSettings();
            setGraphic(tile);
        } else {
            setGraphic(null);
        }
    }
    
    /**
     * When note changes, remove listener, set currentItem and add listener
     * @param note 
     */
    private void updateCurrentItem(Note note) {
        if (currentItem == null || !currentItem.equals(note)) {
            if (currentItem != null) {
                currentItem.titleProperty().removeListener(noteChangeListener);
                currentItem.textProperty().removeListener(noteChangeListener);
            }

            currentItem = note;

            if (currentItem != null) {
                currentItem.titleProperty().addListener(noteChangeListener);
                currentItem.textProperty().addListener(noteChangeListener);
            }
        }
    }
    
    /**
     * Update list tile text property with settings
     */
    private void updateWithSettings() {
        if (currentItem != null) {
            tile.textProperty().setAll(currentItem.getTitle(), 
                                       getSecondLine(currentItem.getText()));
            
            if (service.settingsProperty().get().isShowDate()) {
                tile.textProperty().add(getThirdLine(currentItem.getCreationDate()));
            }
        } else {
            tile.textProperty().clear();
        }
    }
    
    /**
     * Add ellipsis in case more than two lines
     * @param text
     * @return formatted string
     */
    private String getSecondLine(String text) {
        if (text == null || text.isEmpty()) {
            return "";
        }
        
        int eol = text.indexOf("\n", 0);
        return eol < 1 ? text : text.substring(0,eol) + "...";
    }
    
    /**
     * Format date in milliseconds with SimpleDateFormat
     * @param date
     * @return formatted string 
     */
    private String getThirdLine(Long date) {
        return dateFormat.format(new Date(date));
    }
         
}
