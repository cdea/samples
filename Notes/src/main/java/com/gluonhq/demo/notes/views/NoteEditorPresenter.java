package com.gluonhq.demo.notes.views;


import javax.inject.Inject;

import com.gluonhq.demo.notes.model.Model;
import com.gluonhq.demo.notes.model.Note;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import com.gluonhq.demo.notes.Service;
import javafx.beans.binding.Bindings;

/**
 * FXML Controller class for noteseditor.fxml
 *
 */
public class NoteEditorPresenter {
    
    // Injects an instance of service (as singleton)
    // See Afterburner.fx
    @Inject private Service service;
    
    // Injects an instance of model (as singleton)
    // See Afterburner.fx
    @Inject private Model model;
    
    @FXML private View editorView;
    @FXML private Button submit;
    @FXML private Button cancel;
    @FXML private TextField title;
    @FXML private TextArea comment;

    private boolean isEditMode;
    
    public void initialize() {
        
        editorView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                submit.disableProperty().unbind();

                /*
                When the view is shown, if active note is null, view is Create 
                else view is Edit
                */
                Note activeNote = model.activeNote().get();
                isEditMode = model.activeNote().get() != null;
            
                submit.setText( isEditMode ? "APPLY" : "SUBMIT" );
                
                if (isEditMode) {
                    // Set fields with note values
                    title.setText(activeNote.getTitle());
                    comment.setText(activeNote.getText());
                    
                    // Enable submit only if any of the fields have changed
                    submit.disableProperty().bind(Bindings.createBooleanBinding(()->{
                        if (title == null || comment == null) {
                            return true;
                        }
                        return title.textProperty()
                                .isEqualTo(activeNote.getTitle())
                                .and(comment.textProperty()
                                        .isEqualTo(activeNote.getText())).get();
                        }, title.textProperty(),comment.textProperty()));
                } else {
                    // Enable submit only if both fields have content
                    submit.disableProperty().bind(Bindings.createBooleanBinding(() -> {
                            return title.textProperty()
                                    .isEmpty()
                                    .or(comment.textProperty()
                                            .isEmpty()).get();
                        }, title.textProperty(), comment.textProperty()));
                }
                
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.ARROW_BACK.button(e -> close()));
                appBar.setTitleText(isEditMode ? "EDIT NOTE" : "NEW NOTE" );
            } else {
                // reset fields
                title.clear();
                comment.clear();
            }
            
        });
        
        // when changes are submitted
        submit.setOnAction(e -> {
            Note note = isEditMode ? model.activeNote().get() : new Note();
            note.setTitle(title.getText());
            note.setText(comment.getText());
            
            if (!isEditMode) {
                service.addNote(note);
            }
            close();
        });
        
        cancel.setOnAction(e -> close());
    }
    
    /**
     * Reset fields and return to home view
     */
    private void close() {
        title.clear();
        comment.clear();
        model.activeNote().set(null);
        MobileApplication.getInstance().goHome();
    }

}
