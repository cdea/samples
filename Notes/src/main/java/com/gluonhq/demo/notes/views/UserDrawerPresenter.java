package com.gluonhq.demo.notes.views;


import javafx.fxml.FXML;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.SettingsPane;
import com.gluonhq.charm.glisten.control.settings.DefaultOption;
import com.gluonhq.charm.glisten.control.settings.Option;
import com.gluonhq.charm.glisten.control.settings.OptionBase;
import com.gluonhq.charm.glisten.control.settings.OptionEditor;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.notes.Service;
import com.gluonhq.demo.notes.model.Settings;
import java.util.Optional;
import java.util.function.Function;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.Property;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Slider;
import javax.inject.Inject;

/**
 * FXML Controller class for userdrawer.fxml
 *
 */
public class UserDrawerPresenter {

    // Injects an instance of service (as singleton)
    // See Afterburner.fx
    @Inject private Service service;
    
    @FXML private View userDrawer;
    @FXML private SettingsPane settingsPane;
    private Settings settings;
    
    public void initialize() {
        
        settings = new Settings();
        // Set config with initial value of settings
        updateSettings(service.settingsProperty().get());
        
        settings.showDateProperty().addListener((obs, ov, nv) -> updateService());
        settings.fontSizeProperty().addListener((obs, ov, nv) -> updateService());
        settings.sortingProperty().addListener((obs, ov, nv) -> updateService());
        settings.ascendingProperty().addListener((obs, ov, nv) -> updateService());
        
        final DefaultOption<Boolean> dateOption = new DefaultOption(MaterialDesignIcon.DATE_RANGE.graphic(),
                "Show Date", "Show the note's date", null, settings.showDateProperty(), true);
        
        final Option<Number> sliderOption = new SliderOption(
                MaterialDesignIcon.NETWORK_CELL.graphic(), "Size of Text", "Set the text size in the slider", null, 
                settings.fontSizeProperty(), true, 8, 12);
        
        final DefaultOption<Integer> sortOption = new DefaultOption(MaterialDesignIcon.SORT_BY_ALPHA.graphic(),
                "Sort Notes", "Sort the notes by", null, settings.sortingProperty(), true);
        
        final DefaultOption<Boolean> ascendingOption = new DefaultOption(MaterialDesignIcon.SORT.graphic(),
                "Asc./Des.", "Sort in ascending or descending order", null, settings.ascendingProperty(), true);
        
        settingsPane.getOptions().setAll(FXCollections.<Option>observableArrayList(
                dateOption, sliderOption, sortOption, ascendingOption));
        settingsPane.setSearchBoxVisible(false);
        
        // if settings change, update controls values
        service.settingsProperty().addListener((obs,s,s1) -> updateSettings(s1));
        
        userDrawer.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.ARROW_BACK.button(e -> 
                        MobileApplication.getInstance().switchToPreviousView()));
                appBar.setTitleText("Settings");
                appBar.getActionItems().add(MaterialDesignIcon.CLOSE.button(e ->
                        MobileApplication.getInstance().switchToPreviousView()));
            }
        });
    }
    
    /**
     * Update settings with value of given settings
     * @param settings 
     */
    private void updateSettings(Settings settings) {
        if (settings != null) {
            this.settings.setShowDate(settings.isShowDate());
            this.settings.setFontSize(settings.getFontSize());
            this.settings.setSorting(settings.getSorting());
            this.settings.setAscending(settings.isAscending());
        }
    }
    
    /**
     * Persists actual values of settings in service.
     */
    private void updateService() {
        Settings settings = new Settings();
        settings.setShowDate(this.settings.isShowDate());
        settings.setFontSize(this.settings.getFontSize());
        settings.setSorting(this.settings.getSorting());
        settings.setAscending(this.settings.isAscending());
        service.settingsProperty().set(settings);
        
        // save on every change
        service.storeSettings();
    }
    
    /**
     * Custom implementation of OptionBase using a SliderEditor, a Slider control
     */
    private class SliderOption extends OptionBase<Number> {

        private final int min;
        private final int max;
        
        public SliderOption(Node graphic, String caption, String description, String category, IntegerProperty value, 
                boolean isEditable, int min, int max) {
            super(graphic, caption, description, category, (Property<Number>) value, isEditable);
            this.min = min;
            this.max = max;
        }
        
        @Override
        public Property<Number> valueProperty() {
            return value;
        }

        @Override
        public Optional<Function<Option<Number>, OptionEditor<Number>>> editorFactoryProperty() {
            return Optional.of(option -> new SliderEditor(option, min, max));
        }
        
    }
    
    private class SliderEditor implements OptionEditor<Number> {
        
        private final Slider slider;

        public SliderEditor(Option<Number> option, int min, int max) {
            slider = new Slider(min, max, option.valueProperty().getValue().doubleValue());
            slider.setSnapToTicks(true);
            slider.setMajorTickUnit(1);
            slider.setMinorTickCount(0);
            valueProperty().bindBidirectional(option.valueProperty());
        }

        @Override
        public Node getEditor() {
            return slider; 
        }

        @Override
        public Number getValue() {
            return slider.getValue();
        }

        @Override
        public void setValue(Number value) {
            slider.setValue(value.doubleValue());
        }

        @Override
        public final Property<Number> valueProperty() {
            return (Property<Number>) slider.valueProperty();
        }
    }
}
