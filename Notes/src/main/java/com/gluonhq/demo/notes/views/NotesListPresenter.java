package com.gluonhq.demo.notes.views;

import com.gluonhq.charm.down.common.PlatformFactory;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import javax.inject.Inject;

import com.gluonhq.demo.notes.model.Model;
import com.gluonhq.demo.notes.model.Note;
import com.gluonhq.demo.notes.NotesApp;
import com.gluonhq.demo.notes.Service;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.layout.layer.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.notes.model.Settings;
import com.gluonhq.demo.notes.model.Settings.SORTING;
import javafx.collections.ListChangeListener;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Label;

/**
 * FXML Controller class for noteslist.fxml
 *
 */
public class NotesListPresenter {

    // Injects an instance of service (as singleton)
    // See Afterburner.fx
    @Inject private Service service;
    
    // Injects an instance of model (as singleton)
    // See Afterburner.fx
    @Inject private Model model;
    
    @FXML private View view;
    
    // a ListView that will contain the notes
    @FXML private ListView<Note> lstNotes;
    private int currentSize;
    
    // Sorted list to apply sorting criteria from side menu options
    private SortedList<Note> sortedNotes;
    
    public void initialize() {
        
        // placeholder in case list of notes is empty
        final Label label = new Label("There are no notes yet");
        lstNotes.setPlaceholder(label);
        
        // listener to set the list with notes when these are retrieved from storage, wrapped
        // in the sorted list
        service.notesProperty().addListener((ListChangeListener.Change<? extends Note> c) -> {
            sortedNotes = new SortedList<>(c.getList());
            lstNotes.setItems(sortedNotes);
        });
        
        // list cell factory, with consumers for edit and delete buttons
        lstNotes.setCellFactory(e -> 
            new NoteListCell(service,
                             note -> service.removeNote(note),
                             note -> showEdit(note)));
        
        // if settings change or there are changes in the items, sort the list
        service.settingsProperty().addListener((obs,s,s1) -> applySettings());
        lstNotes.itemsProperty().addListener((obs,s,s1) -> applySettings());
        
        // action button, to enter creator view
        final FloatingActionButton floatingActionButton = new FloatingActionButton();
        floatingActionButton.setOnAction(e -> {
            model.activeNote().set(null);
            MobileApplication.getInstance().switchView(NotesApp.NOTE_CREATOR_VIEW);
        });
        view.getLayers().add(floatingActionButton);
        
        view.showingProperty().addListener((obs, ov, nv) -> {
            AppBar appBar = MobileApplication.getInstance().getAppBar();
            // side menu access
            appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                    MobileApplication.getInstance().showLayer(NotesApp.USER_DRAWER)));
            appBar.setTitleText("NOTES");
        });
    }
    
    /**
     * Called when scene is ready to retrieve notes and settings
     */
    public void postInit() {
        service.retrieveNotes();
    }
    
    /**
     * consumer added to every cell, allowing entering edit mode 
     * @param note 
     */
    private void showEdit(Note note) {
        model.activeNote().set(note);
        MobileApplication.getInstance().switchView(NotesApp.NOTE_EDIT_VIEW);   
    }
    
    /**
     * Sort listView of Notes based on defined criterion in settings
     */
    private void applySettings() {
        if (sortedNotes != null) {
            final Settings settings = service.settingsProperty().get();
            
            if (currentSize != settings.getFontSize()) { 
                currentSize = settings.getFontSize();
                lstNotes.setStyle("-fx-font-size: " + 
                        ((PlatformFactory.getPlatform().isTablet() ? 1.4 : 1.0) * currentSize) + "pt;");
            }
            
            switch (SORTING.byId(settings.getSortingId())) {
                case DATE:  
                    if (settings.isAscending()) {
                        sortedNotes.setComparator((n1, n2) -> Long.compare(n1.getCreationDate(), n2.getCreationDate()));
                    } else {
                        sortedNotes.setComparator((n1, n2) -> Long.compare(n2.getCreationDate(), n1.getCreationDate()));
                    }
                    break;
                case TITLE: 
                    if (settings.isAscending()) {
                        sortedNotes.setComparator((n1, n2) -> n1.getTitle().compareTo(n2.getTitle()));
                    } else {
                        sortedNotes.setComparator((n1, n2) -> n2.getTitle().compareTo(n1.getTitle()));
                    }
                    break;
                case CONTENT: 
                    if (settings.isAscending()) {
                        sortedNotes.setComparator((n1, n2) -> n1.getText().compareTo(n2.getText()));
                    } else {
                        sortedNotes.setComparator((n1, n2) -> n2.getText().compareTo(n1.getText()));
                    }
                    break;
            }
        }
    }
    
}
