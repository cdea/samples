package com.gluonhq.codevault;

import com.gluonhq.codevault.util.Octicons;
import com.gluonhq.particle.application.ParticleApplication;
import javafx.scene.Scene;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import static org.controlsfx.control.action.ActionMap.actions;

public class CodeVaultApp extends ParticleApplication {

    public static void main(String[] args) {
        GlyphFontRegistry.register(new Octicons());
        launch(args);
    }

    public CodeVaultApp() {
        super("CodeVault");
    }

    @Override
    public void postInit(Scene scene) {
        scene.getStylesheets().add(CodeVaultApp.class.getResource("/gitview.css").toExternalForm());

        setTitle( "Code Vault" );

        getParticle().buildMenu(
                "File -> [openRepo,---, exit]"
        );

        // build toolbar
        getParticle().getToolBarActions().addAll(
                actions("openRepo")
        );
    }

}
