package com.gluonhq.codevault;

import com.gluonhq.codevault.view.RepoManagerView;
import com.gluonhq.particle.annotation.ParticleActions;
import com.gluonhq.particle.application.ParticleApplication;
import com.gluonhq.particle.view.View;
import com.gluonhq.particle.view.ViewManager;
import javafx.stage.DirectoryChooser;
import org.controlsfx.control.action.ActionProxy;

import javax.inject.Inject;
import java.util.Optional;

@ParticleActions
public class MenuActions {

    @Inject private ParticleApplication particleApp;

    @Inject private ViewManager viewManager;

    @ActionProxy(text="Exit",
            graphic="font>github-octicons|SIGN_OUT",
            accelerator="alt+F4")
    private void exit() {
        particleApp.exit();
    }


    @ActionProxy(
            text="Open Repository",
            graphic="font>FontAwesome|FOLDER_OPEN",
            accelerator="ctrl+O")
    private void openRepo() {

        View currentView = viewManager.getCurrentView();

        if ( currentView instanceof RepoManagerView) {
            RepoManagerView view = (RepoManagerView) currentView;
            DirectoryChooser dirChooser = new DirectoryChooser();
            dirChooser.setTitle("Open Repository");
            Optional.ofNullable(dirChooser.showDialog(particleApp.getPrimaryStage())).ifPresent(view::openRepo);
        }
    }

}
