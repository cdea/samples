package com.gluonhq.codevault.view;

import com.gluonhq.codevault.git.*;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


public class RepLogController implements Initializable {

    @FXML public TableView<GitCommit> table;

    @FXML public TableColumn<GitCommit, String> tcCommit;
    @FXML public TableColumn<GitCommit, GitCommit> tcDescription;
    @FXML public TableColumn<GitCommit, String> tcAuthor;
    @FXML public TableColumn<GitCommit, Date> tcDate;

    @FXML public TreeView info;

    private GitRepository repo;


    // repositoryProperty
    private final ObjectProperty<GitRepository> repositoryProperty = new SimpleObjectProperty<GitRepository>(this, "repository");

    public final ObjectProperty<GitRepository> repositoryProperty() {
       return repositoryProperty;
    }

    public final GitRepository getRepository() {
       return repositoryProperty.get();
    }

    public final void setRepository(GitRepository value) throws GitRepoException {
        repositoryProperty.set(value);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SplitPane.setResizableWithParent(info, false);

        info.setCellFactory( tree -> new InfoTreeCell());

        tcDate.setCellValueFactory( new PropertyValueFactory("time"));
        tcAuthor.setCellValueFactory( new PropertyValueFactory("author"));
        tcCommit.setCellValueFactory( new PropertyValueFactory("hash"));

        tcDescription.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        tcDescription.setCellFactory(column -> new CommitDescriptionTableCell());

        repositoryProperty.addListener((observable, oldValue, repo) -> {
            if (repo != null) {
                table.getItems().setAll(repo.getLog());
                info.setRoot(createRepoInfoModel());
            } else {
                table.getItems().clear();
                info.setRoot(null);
            }
        });

    }

    private TreeItem<GitRef> createRepoInfoModel() {

        GitRepository repo = getRepository();

        TreeItem<GitRef> branches = createTopicFromElements("Branches", repo.getBranches(), true);

        List<TreeItem<GitRef>> topics = Arrays.asList(
//                createTopicFromElements("File Status", List(new Workspace()), true),
                branches,
                createTopicFromElements("Tags", repo.getTags(), false)
//                createTopicFromElements("Remotes", repo.getRemotes(), true)
        );

        return createTopicFromTreeItems(repo.getName(), topics, true);
    }

    private static TreeItem<GitRef> createTopicFromElements( String name, Collection<? extends GitRef> children, boolean expand ) {
        return createTopicFromTreeItems(name, children.stream().map( c-> new TreeItem<GitRef>(c)).collect(Collectors.toList()), expand);
    }

    private static TreeItem<GitRef>createTopicFromTreeItems( String name, Collection<TreeItem<GitRef>> children, Boolean expand ) {
        TreeItem<GitRef> result = new TreeItem<GitRef>(new Topic(name));
        result.setExpanded(expand);
        result.getChildren().addAll(children);
        return result;
    }



}

