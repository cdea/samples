package com.gluonhq.codevault.view;

import com.gluonhq.codevault.git.GitRef;
import com.gluonhq.codevault.git.Topic;
import com.gluonhq.codevault.util.UITools;
import javafx.scene.control.TreeCell;

public class InfoTreeCell extends TreeCell<GitRef> {

    private String lastStyle;

    {
        getStyleClass().add("repoViewCell");
    }

    @Override
    protected void updateItem(GitRef ref, boolean empty) {
        super.updateItem(ref, empty);
        if ( empty ) {
            setText(null);
            setGraphic(null);
        } else {


            if ( ref instanceof Topic) {
                lastStyle = "topic";
                getStyleClass().add(lastStyle);
            } else {
              getStyleClass().remove(lastStyle);
            }

            setText(ref.getShortName());
            setGraphic(UITools.getRefIcon(ref));
        }
    }
}
