
GluonSQLite
===========

A JavaFX Application that shows how to embed an SQLite database on your project.

- You can bundle an existing database file as a resource
- You can create by code a new database
- You can perform any query over the resulting database

It uses the proper SQLite dependency and driver for each platform, and uses Gluon Charm Down to 
access the private storage location on them

Instructions
------------
To execute the sample, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`
* Desktop
> `./gradlew run`
* Embedded
> Configure your Raspberry Pi, add the settings to the `build.gradle` script and run `./gradlew runEmbedded`

Blog Post
---------

Read in more detail about this sample [here](http://gluonhq.com/embedding-database-cross-platform-projects/)

