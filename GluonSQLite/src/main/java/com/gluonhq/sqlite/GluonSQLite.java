package com.gluonhq.sqlite;

import com.gluonhq.charm.down.common.JavaFXPlatform;
import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.sqlite.model.Person;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GluonSQLite extends MobileApplication {

    private final static String DB_NAME = "sample.db";
    
    private VBox root;
    private ListView<String> status;
    
    private Connection connection = null;
    private Statement stmt;
    private ResultSet rs;
    
    @Override
    public void init(){
        long timeIni = System.currentTimeMillis();
        try {
            Class c = null;
            if (JavaFXPlatform.isAndroid()) {
                c = Class.forName("org.sqldroid.SQLDroidDriver");
            } else if (JavaFXPlatform.isIOS()) {
                c = Class.forName("SQLite.JDBCDriver");
            } else if (JavaFXPlatform.isDesktop()) {
                c = Class.forName("org.sqlite.JDBC");
            } else if (System.getProperty("os.arch").toUpperCase().contains("ARM")) {
                c = Class.forName("org.sqlite.JDBC");
            }
            System.out.println("Got driver class: " + c);
        } catch (ClassNotFoundException e) {
            System.out.println("Error class not found " + e);
        }
        
        System.out.println("Load time: "+(System.currentTimeMillis() - timeIni) + " ms");
        
        addViewFactory(HOME_VIEW, () -> new View(HOME_VIEW) {
            
            {
                ListView<Person> listView = new ListView<>();
                listView.setPlaceholder(new Label("No items yet"));
                listView.setCellFactory(param -> {
                    return new ListCell<Person>() {
                        @Override 
                        protected void updateItem(Person item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null && !empty) {
                                setText(item.getFirstName() + " " + item.getLastName());
                            } else {
                                setText(null);
                            }
                        }
                    };
                });

                final Label labelDB = new Label("DB Items");
                labelDB.setStyle("-fx-text-fill: gray");
                VBox vListDB = new VBox(10, labelDB, listView);
                vListDB.setAlignment(Pos.CENTER_LEFT);

                status = new ListView<>();

                final Label labelStatus = new Label("Log");
                labelStatus.setStyle("-fx-text-fill: gray");
                VBox vListStatus = new VBox(10, labelStatus, status);
                vListStatus.setAlignment(Pos.CENTER_LEFT);

                root = new VBox(20);
                root.setPadding(new Insets(10));
                root.setAlignment(Pos.CENTER);

                Button button1 = new Button("Load DB");
                button1.setOnAction(e -> {
                    listView.getItems().clear();
                    final List<Person> list = readDB();
                    listView.setItems(FXCollections.observableArrayList(list));
                });

                Button button2 = new Button("Create DB");
                button2.setOnAction(e -> {
                    listView.getItems().clear();
                    final List<Person> list = createDB();
                    listView.setItems(FXCollections.observableArrayList(list));
                });

                Button button3 = new Button("Clear");
                button3.setOnAction(e -> {
                    listView.getItems().clear();
                    status.getItems().clear();
                });

                final HBox hBox = new HBox(30, button1, button2, button3);
                hBox.setMinHeight(50);
                hBox.setAlignment(Pos.CENTER);

                root.getChildren().addAll(hBox, vListDB, vListStatus);
                setCenter(root);

                showingProperty().addListener((obs, ov, nv) -> {
                    if (nv) {
                        AppBar appBar = MobileApplication.getInstance().getAppBar();
                        appBar.setNavIcon(MaterialDesignIcon.PERSON_PIN.button());
                        appBar.setTitleText("Gluon Mobile & SQLite");
                    }
                });
            }
            
        });
    }
    
    @Override
    public void postInit(Scene scene) {
        Swatch.AMBER.assignTo(scene);

        if (JavaFXPlatform.isDesktop()) {
            ((Stage) scene.getWindow()).getIcons().add(new Image(GluonSQLite.class.getResourceAsStream("/icon.png")));
        } 
        
        if (System.getProperty("os.arch").toUpperCase().contains("ARM") && 
                !JavaFXPlatform.isIOS()  && !JavaFXPlatform.isAndroid()) {
            System.out.println("Pi!!");
            ((Stage) scene.getWindow()).setFullScreen(true);
            ((Stage) scene.getWindow()).setFullScreenExitHint("");
        }

    }
    
    @Override
    public void stop() {
        try { 
            if (connection != null) {
                connection.close();
            } 
        } catch (SQLException ex) { 
            System.out.println("SQL error " + ex);
        }
    }
    
    private List<Person> createDB() {
        List<Person> list = new ArrayList<>();
        try {
            File dir;
            String dbUrl = "jdbc:sqlite:";
            try {
                dir = PlatformFactory.getPlatform().getPrivateStorage();
                File db = new File (dir, DB_NAME);
                dbUrl = dbUrl + db.getAbsolutePath();
            } catch (IOException ex) {
                System.out.println("Error " + ex);
            }

            try {
                connection = DriverManager.getConnection(dbUrl);
                status.getItems().add("Connection established: "+dbUrl);
                System.out.println("db connection ok");
            } catch (SQLException ex) {
                System.out.println("Error establishing connection " +ex);
            }                

            if (connection != null) {
                stmt = connection.createStatement();
                stmt.setQueryTimeout(30);  // set timeout to 30 sec.

                status.getItems().add("Creating table 'person'...");
                stmt.executeUpdate("drop table if exists person");
                stmt.executeUpdate("create table person (id integer, firstname string, lastname string)");
                stmt.executeUpdate("insert into person values(1, 'Johan', 'Vos')");
                stmt.executeUpdate("insert into person values(2, 'Eugene', 'Ryzhikov')");
                stmt.executeUpdate("insert into person values(3, 'Joeri', 'Sykora')");
                stmt.executeUpdate("insert into person values(4, 'Erwin', 'Morrhey')");

                status.getItems().add("Retrieving results from table 'person'...");
                rs = stmt.executeQuery("select * from person");
                while (rs.next()) {
                    String firstname = rs.getString("firstname");
                    String lastname = rs.getString("lastname");
                    list.add(new Person(firstname, lastname));
                }
                System.out.println("SQL ok");
            }
        } catch (SQLException ex) {
            System.out.println("SQL error " + ex);
        } finally {
            try { 
                if (rs != null) {
                    rs.close();
                } 
            } catch (SQLException ex) { System.out.println("SQL error " + ex); }
            try { 
                if (stmt != null) {
                    stmt.close();
                } 
            } catch (SQLException ex) { System.out.println("SQL error " + ex); }
        }
        return list;
    }

    private List<Person> readDB() {
        List<Person> list = new ArrayList<>();
        
        String dbUrl = "jdbc:sqlite:";
        if (JavaFXPlatform.isDesktop()) {
            // desktop (xerial)
            dbUrl= dbUrl +":resource:" + GluonSQLite.class.getResource("/databases/" + DB_NAME).toExternalForm();
        } else {
            File dir;
            try {
                dir = PlatformFactory.getPlatform().getPrivateStorage();
                File db = new File (dir, DB_NAME);
                status.getItems().add("Copying database " + DB_NAME + " to private storage");
                System.out.println("Copying database " + DB_NAME + " to private storage");
                copyDatabase("/databases/", dir.getAbsolutePath(), DB_NAME);
                dbUrl = dbUrl + db.getAbsolutePath();
            } catch (IOException ex) { 
                System.out.println("IO error " + ex);
            }
        }
        try {
            connection = DriverManager.getConnection(dbUrl);
            status.getItems().add("Connection established: " + dbUrl);
            System.out.println("Connection established: " + dbUrl);
        } catch (SQLException ex) {
            System.out.println("Error establishing connection " +ex);
        }

        try {
            DatabaseMetaData md = connection.getMetaData();
            rs = md.getTables(null, null, "%", null);
            status.getItems().add("Tables in Database " + DB_NAME);
            System.out.println("Tables in Database " + DB_NAME);
            while (rs.next()) {
                System.out.println(rs.getString(3));
                status.getItems().add(" * " +rs.getString(3));
            }
            
            status.getItems().add("Reading table person");
            System.out.println("Reading table person");
            stmt = connection.createStatement();
            rs = stmt.executeQuery("select * from person");
            while (rs.next()) {
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                list.add(new Person(firstname, lastname));
            }
        } catch (SQLException ex) {
            System.out.println("SQL error " + ex); 
        } finally {
            try { 
                if (rs != null) {
                    rs.close();
                } 
            } catch (SQLException ex) { System.out.println("SQL error " + ex); }
            try { 
                if (stmt != null) {
                    stmt.close();
                } 
            } catch (SQLException ex) { System.out.println("SQL error " + ex); }
        }
        return list;
    }
    
    
    private void copyDatabase(String pathIni, String pathEnd, String name)  {
        
        try (InputStream myInput = GluonSQLite.class.getResourceAsStream(pathIni+name)) {
            String outFileName =  pathEnd + "/" + name;
            try (OutputStream myOutput = new FileOutputStream(outFileName)) {
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }
                myOutput.flush();
                
            } catch (IOException ex) {
                System.out.println("Error " + ex);
            }
        } catch (IOException ex) {
            System.out.println("Error " + ex);
        } 
    }
}
