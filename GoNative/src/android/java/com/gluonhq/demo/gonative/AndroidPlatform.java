package com.gluonhq.demo.gonative;

public class AndroidPlatform extends GoNativePlatform {

    private AndroidNativeService nativeService;
        
    @Override
    public NativeService getNativeService() {
        if(nativeService == null){
            nativeService = new AndroidNativeService();
        }
        return nativeService;
    }
}
