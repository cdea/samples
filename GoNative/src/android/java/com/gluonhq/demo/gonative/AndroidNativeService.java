package com.gluonhq.demo.gonative;

import static android.app.Activity.RESULT_OK;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import java.io.File;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafxports.android.FXActivity;

public class AndroidNativeService implements NativeService {

    private static final int SELECT_PICTURE = 1;
    private static final int TAKE_PICTURE = 2;    

    private final ObjectProperty<Image> fxImage = new SimpleObjectProperty<>(
            new Image(getClass().getResourceAsStream("/icon.png")));


    @Override
    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        final Uri imageUri = Uri.fromFile(photo);

        FXActivity.getInstance().setOnActivityResultHandler((requestCode, resultCode, data) -> {
            if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
                String selectedImagePath = getPath(FXActivity.getInstance(), imageUri);
                Uri converted = Uri.fromFile(new File(selectedImagePath));
                fxImage.set(new Image(converted.toString()));
            }
        });

        // check for permissions
        if (intent.resolveActivity(FXActivity.getInstance().getPackageManager()) != null) {
            FXActivity.getInstance().startActivityForResult(intent, TAKE_PICTURE);
        } else {
            Log.e("GalleryActivity", "resolveActivity failed");
        }
    }

    @Override
    public void retrievePicture() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        
        FXActivity.getInstance().setOnActivityResultHandler((requestCode, resultCode, data) -> {
            if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                String selectedImagePath = getPath(FXActivity.getInstance(), selectedImageUri);
                if (selectedImagePath != null && !selectedImagePath.isEmpty()) {
                    Uri converted = Uri.fromFile(new File(selectedImagePath));
                    fxImage.set(new Image(converted.toString()));
                }
            }
        });
        
        // check for permissions
        if (intent.resolveActivity(FXActivity.getInstance().getPackageManager()) != null) {
            FXActivity.getInstance().startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
        } else {
            Log.e("GalleryActivity", "resolveActivity failed");
        }
    }

    @Override
    public ObjectProperty<Image> imageProperty() {
        return fxImage;
    }
    
    @Override
    public void vibrate() {
        Vibrator v = (Vibrator) FXActivity.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
    }
    
    /*
    Accelerometer
    */
    
    private final Accelerometer accelerometer = new Accelerometer();
    
    @Override
    public void startAccelerometer() {
        accelerometer.start();
    }
    
    @Override
    public void stopAccelerometer() {
        accelerometer.stop();
    }
    
    @Override
    public DoubleProperty x() {
        return accelerometer.x();
    }

    @Override
    public DoubleProperty y() {
        return accelerometer.y();
    }

    @Override
    public DoubleProperty z() {
        return accelerometer.z();
    }
    
    /*** private methods ***/
    
    // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
    private String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
        
    private String getDataColumn(Context context, Uri uri, String selection,
            String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
