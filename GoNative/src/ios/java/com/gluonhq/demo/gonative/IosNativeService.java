package com.gluonhq.demo.gonative;

import com.gluonhq.charm.down.common.PicturesService;
import com.gluonhq.charm.down.common.PlatformFactory;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import org.robovm.apple.audiotoolbox.AudioServices;

public class IosNativeService implements NativeService {

    private final ObjectProperty<Image> fxImage = new SimpleObjectProperty<>(
            new Image(getClass().getResourceAsStream("/icon.png")));
    
    private final PicturesService picturesService;

    public IosNativeService() {
        picturesService = PlatformFactory.getPlatform().getPicturesService();
    }

    
    @Override
    public void takePicture() {
        picturesService
                .retrievePicture(PicturesService.PictureSource.CAMERA)
                .addListener((obs, ov, nv) -> {
                   fxImage.set(nv);
                });
    }

    @Override
    public void retrievePicture() {
        picturesService
                .retrievePicture(PicturesService.PictureSource.GALLERY)
                .addListener((obs, ov, nv) -> {
                   fxImage.set(nv);
                });
    }

    @Override
    public ObjectProperty<Image> imageProperty() {
        return fxImage;
    }
    
    @Override
    public void vibrate() {
        AudioServices.playSystemSound(AudioServices.SystemSoundVibrate);
    }
    
    @Override
    public void startAccelerometer() {
        System.out.println("Start Accelerometer");
    }
    
    @Override
    public void stopAccelerometer() {
        System.out.println("Stop Accelerometer");
    }
    
    @Override
    public DoubleProperty x() {
        return new SimpleDoubleProperty();
    }

    @Override
    public DoubleProperty y() {
        return new SimpleDoubleProperty();
    }

    @Override
    public DoubleProperty z() {
        return new SimpleDoubleProperty();
    }
}
