package com.gluonhq.demo.gonative;

public class IosPlatform extends GoNativePlatform {

    private IosNativeService nativeService;
        
    @Override
    public NativeService getNativeService() {
        if(nativeService == null){
            nativeService = new IosNativeService();
        }
        return nativeService;
    }
    
}
