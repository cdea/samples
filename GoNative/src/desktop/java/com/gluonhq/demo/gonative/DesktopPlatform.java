package com.gluonhq.demo.gonative;

public class DesktopPlatform extends GoNativePlatform {

    private DesktopNativeService nativeService;
        
    @Override
    public NativeService getNativeService() {
        if(nativeService == null){
            nativeService = new DesktopNativeService();
        }
        return nativeService;
    }
    
}
