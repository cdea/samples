package com.gluonhq.demo.gonative;

import java.io.File;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class DesktopNativeService implements NativeService {

    private final ObjectProperty<Image> fxImage = new SimpleObjectProperty<>(
            new Image(getClass().getResourceAsStream("/icon.png")));
    
    @Override
    public void takePicture() {
    }

    @Override
    public void retrievePicture() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        fileChooser.setTitle("Open a Picture...");

        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            fxImage.set(new Image("file://"+file.getAbsolutePath()));
        }
    }

    @Override
    public ObjectProperty<Image> imageProperty() {
        return fxImage;
    }
 
    @Override
    public void vibrate() {
        System.out.println("Vibrating!");
    }
    
    @Override
    public void startAccelerometer() {
        System.out.println("Start Accelerometer");
    }
    
    @Override
    public void stopAccelerometer() {
        System.out.println("Stop Accelerometer");
    }

    @Override
    public DoubleProperty x() {
        return new SimpleDoubleProperty();
    }

    @Override
    public DoubleProperty y() {
        return new SimpleDoubleProperty();
    }

    @Override
    public DoubleProperty z() {
        return new SimpleDoubleProperty();
    }
    
}
