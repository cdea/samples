package com.gluonhq.demo.gonative;

public abstract class GoNativePlatform {
   
    public abstract NativeService getNativeService();

}
