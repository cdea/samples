package com.gluonhq.demo.gonative;

import com.gluonhq.charm.down.common.JavaFXPlatform;
import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.CardPane;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

public class GoNativeFX extends MobileApplication {

    private AppBar appBar;
    
    @Override
    public void init() {
        
        final NativeService nativeService = GoNativePlatformFactory.getPlatform().getNativeService();

        addViewFactory(HOME_VIEW, () -> {
            View homeView = new View(HOME_VIEW) {
                {
                    final TabPane tabs = new TabPane();
                    tabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

                    /*
                    Tab 1. Pictures
                    */
                    final CardPane cardPics = new CardPane();
                    
                    final ImageView imageView = new ImageView();
                    imageView.setImage(nativeService.imageProperty().get());
                    imageView.fitWidthProperty().bind(widthProperty().subtract(60));
                    imageView.fitHeightProperty().bind(heightProperty().subtract(220));
                    imageView.setPreserveRatio(true);
                    cardPics.getCards().add(imageView);
                    
                    final Button buttonGallery = new Button("Select a Picture", MaterialDesignIcon.COLLECTIONS.graphic());
                    buttonGallery.setOnAction(e -> nativeService.retrievePicture());

                    final Button buttonCamera = new Button("Take a Picture", MaterialDesignIcon.CAMERA.graphic());
                    buttonCamera.setDisable(JavaFXPlatform.isDesktop());
                    buttonCamera.setOnAction(e -> nativeService.takePicture());
                    
                    HBox hBox = new HBox(20, buttonGallery, buttonCamera);
                    hBox.setMinHeight(50);
                    hBox.setPrefHeight(50);
                    hBox.setMaxHeight(50);
                    hBox.setAlignment(Pos.CENTER);

                    VBox.setVgrow(imageView, Priority.ALWAYS);
                    VBox root1 = new VBox(20.0, hBox, cardPics);
                    root1.setPadding(new Insets(20));
                    root1.setAlignment(Pos.CENTER);
                    
                    nativeService.imageProperty().addListener((obs, ov, nv) -> imageView.setImage(nv));
                    
                    /*
                    Tab 2. Vibrator
                    */
                    final Button button = new Button("Click me!");
                    button.setOnAction(e -> nativeService.vibrate());

                    StackPane root2 = new StackPane(button);

                    /*
                    Tab 3. Accelerometer
                    */
                    final CardPane cardChart = new CardPane();
                    
                    final ToggleButton switchAccel = new ToggleButton("Accelerometer");
                    switchAccel.getStyleClass().add("switch");
                    switchAccel.selectedProperty().addListener((obs, ov, nv) -> {
                        if (nv) {
                            nativeService.startAccelerometer();
                        } else {
                            nativeService.stopAccelerometer();
                        }
                    });
                    
                    PlatformFactory.getPlatform().setOnLifecycleEvent(param -> {
                        switch (param) {
                            case PAUSE:
                                    if (switchAccel.isSelected()) {
                                        nativeService.stopAccelerometer();
                                    }
                                    break;
                            case RESUME:
                                    if (switchAccel.isSelected()) {
                                        nativeService.startAccelerometer();
                                    }
                                    break;
                        }
                        return null;
                    });
                    
                    HBox hSwitch = new HBox(10, new Label("Enable:"), switchAccel);
                    hSwitch.setAlignment(Pos.CENTER_LEFT);
                    hSwitch.setPadding(new Insets(0,10,0,10));
                            
                    NumberAxis xAxis = new NumberAxis();
                    xAxis.setAutoRanging(true);
                    xAxis.setForceZeroInRange(false);
                    xAxis.setTickLabelFormatter(new StringConverter<Number>(){

                        @Override
                        public String toString(Number t) {
                            return new SimpleDateFormat("HH:mm:ss").format(new Date(t.longValue()));
                        }

                        @Override
                        public Number fromString(String string) {
                            throw new UnsupportedOperationException("Not supported yet.");
                        }

                    });
                    
                    final int tamMax=200;
                    NumberAxis yAxis = new NumberAxis();
                    LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
                    chart.setPrefSize(300, 360);
                    chart.setCreateSymbols(false);
                    chart.setAnimated(false);
                    XYChart.Series<Number, Number> x1Series = new XYChart.Series<>();
                    XYChart.Series<Number, Number> y1Series = new XYChart.Series<>();
                    XYChart.Series<Number, Number> z1Series = new XYChart.Series<>();
                    x1Series.setName("X-Axis");
                    y1Series.setName("Y-Axis");
                    z1Series.setName("Z-Axis");
                    chart.getData().addAll(x1Series,y1Series,z1Series);
                    nativeService.x().addListener((obs, ov, nv) -> {
                        x1Series.getData().add(new XYChart.Data<>(System.currentTimeMillis(), nv));
                        if (x1Series.getData().size() > tamMax) {
                            x1Series.getData().remove(0);
                        }
                    });
                    nativeService.y().addListener((obs, ov, nv) -> {
                        y1Series.getData().add(new XYChart.Data<>(System.currentTimeMillis(), nv));
                        if (y1Series.getData().size() > tamMax) {
                            y1Series.getData().remove(0);
                        }
                    });
                    nativeService.z().addListener((obs, ov, nv) -> {
                        z1Series.getData().add(new XYChart.Data<>(System.currentTimeMillis(), nv));
                        if (z1Series.getData().size() > tamMax) {
                            z1Series.getData().remove(0);
                        }
                    });
                    
                    VBox vbox = new VBox(10, hSwitch, chart);
                    vbox.setAlignment(Pos.CENTER);
                    VBox.setVgrow(chart, Priority.ALWAYS);
                    vbox.setPadding(new Insets(10));
                    cardChart.getCards().addAll(vbox);
                    
                    final Tab tabAccel = new Tab("Accelerometer", cardChart);
                    tabAccel.setDisable(JavaFXPlatform.isIOS());
                    
                    tabs.getTabs().addAll(new Tab("Pictures", root1), 
                            new Tab("Vibrator", root2), 
                            tabAccel);
                    
                    setCenter(tabs);
                    
                    /* 
                    AppBar
                    */
                    showingProperty().addListener((obs, ov, nv) -> {
                        if (nv) {
                            appBar = MobileApplication.getInstance().getAppBar();
                            appBar.setNavIcon(MaterialDesignIcon.PICTURE_IN_PICTURE.button());
                            appBar.setTitleText("Go Native");
                            
                        }
                    });
                    
                    tabs.getSelectionModel().selectedIndexProperty().addListener((obs, i, i1) -> {
                        switch(i1.intValue()) {
                            case 0: appBar.setNavIcon(MaterialDesignIcon.PICTURE_IN_PICTURE.button());
                                break;
                            case 1: appBar.setNavIcon(MaterialDesignIcon.VIBRATION.button());
                                break;
                            case 2: appBar.setNavIcon(MaterialDesignIcon.LEAK_ADD.button());
                                break;
                        }
                    });
                    
                    
                }
            };
            return homeView;
        });
    }
    
    @Override
    public void postInit(Scene scene) {
        Swatch.TEAL.assignTo(scene);
        scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
    }

}
