package com.gluonhq.demo.gonative;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.image.Image;

public interface NativeService {
    
    /**
     * Pictures
     */
    void takePicture();
    
    void retrievePicture();
    
    ObjectProperty<Image> imageProperty();
    
    /**
     * Vibrator
     */
    void vibrate();
    
    /**
     * Accelerometer
     */
    void startAccelerometer();
    
    void stopAccelerometer();
    
    DoubleProperty x();
    DoubleProperty y();
    DoubleProperty z();
}
