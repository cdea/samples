package com.gluonhq.demos.comments.cloud;

import com.gluonhq.charm.connect.Authentication;
import com.gluonhq.demos.comments.model.Comment;
import com.gluonhq.charm.connect.GluonClient;
import com.gluonhq.charm.connect.User;
import com.gluonhq.charm.connect.service.CharmObservableList;
import com.gluonhq.charm.connect.service.StorageService;
import com.gluonhq.charm.connect.service.StorageWhere;
import com.gluonhq.charm.connect.service.SyncFlag;
import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.down.common.cache.Cache;
import com.gluonhq.charm.down.common.cache.CacheManager;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import javax.annotation.PostConstruct;

/** Service to access the application on Gluon Cloud.
 * 
 * Retrieves a list with comments and sends new or edited comments to the list.
 * 
 */
public class CommentsService {

    private GluonClient gluonClient;
    /*
    The StorageService can be used to retrieve data from and store data to a storage location
    */
    private StorageService storageService;
    
    /*
    Every list stored under the same application on the Cloud has a unique id:
    */
    private static final String CLOUD_LIST_ID = "comments v2.0";
    
    /*
    An observable wrapper of the retrieved list, used to expose it and bind the 
    ListView items to the list.
    */
    private final ListProperty<Comment> commentsList = 
            new SimpleListProperty<>(FXCollections.<Comment>observableArrayList()); 
    
    /*
    Contains a comment that can be edited 
    */
    private final ObjectProperty<Comment> editComment = new SimpleObjectProperty<>();
    
    // Status of connection to cloud
    private final BooleanProperty connected = new SimpleBooleanProperty();
    
    /*
    The authenticated user. Check
    http://docs.gluonhq.com/charm/latest/#_user_authentication
    */
    private final ObjectProperty<User> user = new SimpleObjectProperty<>();
    
    /**
     * Cache to manage avatar images
     */
    private static final Cache<String, Image> cache;
    static {
        CacheManager cacheManager = PlatformFactory.getPlatform().getCacheManager();
        cache = cacheManager.<String, Image>createCache("images");
    }
    
    public static Image getUserImage (String userPicture) {
        if (userPicture == null || userPicture.isEmpty()) {
            /**
             * https://commons.wikimedia.org/wiki/File:WikiFont_uniE600_-_userAvatar_-_blue.svg
             * By User:MGalloway (WMF) (mw:Design/WikiFont) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons
             */
            userPicture = CommentsService.class.getResource("WikiFont_uniE600_-_userAvatar_-_blue.svg.png").toExternalForm();
        }
        // try to retrieve image from cache
        Image answer = cache.get(userPicture);
        if (answer == null) {
            // if not available yet, create new image from URL 
            answer = new Image(userPicture, true);
            // store it in cache
            cache.put(userPicture, answer);
        }
        return answer;
    }
    
    /**
     * See Afterburner.fx
     */
    @PostConstruct
    public void postConstruct() {
        gluonClient = GluonClientProvider.getGluonClient();
        // retrieve a valid storage service and force user authentication
        storageService = gluonClient.getStorageService().authentication(Authentication.USER);
        // bind properties so a listener can be added to react to changes
        user.bind(gluonClient.getAuthenticationService().authenticatedUserProperty());
    }
    
    /**
     * Once there's a valid storageService, the contents of the list can be retrieved. This will return a 
     * CharmObservableList. Note the flags:
     * - LIST_READ_THROUGH: Changes in the remote list on Gluon Cloud are reflected to the local copy of that list
     * - LIST_WRITE_THROUGH: Changes in the local list are reflected to the remote copy of that list on Gluon Cloud
     * - OBJECT_READ_THROUGH: Changes in observable properties of objects in the remote list on Gluon Cloud are reflected to the local objects of that list
     * - OBJECT_WRITE_THROUGH: Changes in the observable properties of objects in the local list are reflected to the remote copy on Gluon Cloud
     *
     * This means that any change done in any client app will be reflected in the cloud and immediately broadcast
     * to all the listening applications.
     */
    public void retrieveComments(){
        // A List<Comment> stored in Gluon Cloud, it can be retrieved as a CharmObservableList
        // if user is not authenticated yet, the next statement will trigger the authentication view
        
        // Following this http://docs.gluonhq.com/charm/latest/#_user_authentication
        // login here http://portal.gluonhq.com/rest/login and add login methods to your application
        
        CharmObservableList<Comment> retrieveList = storageService.<Comment>retrieveList(CLOUD_LIST_ID, Comment.class, 
                StorageWhere.GLUONCLOUD,
                SyncFlag.LIST_READ_THROUGH, SyncFlag.LIST_WRITE_THROUGH, 
                SyncFlag.OBJECT_READ_THROUGH, SyncFlag.OBJECT_WRITE_THROUGH);
        
        if (retrieveList.getException() != null) {
            System.out.println("Exception: "+retrieveList.getException().getMessage());
            connected.set(false);
        }
        
        retrieveList.stateProperty().addListener((ov, s, s1) -> {
            if (s1.equals(CharmObservableList.State.INITIALIZED)) {
                // add the elements to the list
                commentsList.set(retrieveList);
                connected.set(true);
            }
        });
    }
    
    /**  
     * Add a new comment to the list
     * Note comments can be deleted directly on the ListView, since its bounded to the list
     * @param comment
     */
    public void addComment(Comment comment){
        commentsList.get().add(comment);
    }
    
    /**
     *
     * @return: the wrapper of the remote list of comments.
     */
    public ListProperty<Comment> commentsProperty(){
        return commentsList;
    }
    
    /** Allows selection of a comment to be edited
     * 
     * @param comment 
     */
    public void setEditComment(Comment comment){
        editComment.set(comment);
    }
    
    /**
     * 
     * @return the edited comment 
     */
    public Comment getEditComment() {
        return editComment.get();
    }
    
    /**
     * 
     * @return the status of the cloud connection 
     */
    public BooleanProperty connected() {
        return connected;
    }
    
    /**
     * 
     * @return the authenticated user
     */
    public User getUser() {
        return user.get();
    }
    
    public ObjectProperty<User> user() {
        return user;
    }
}
