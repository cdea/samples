package com.gluonhq.demos.comments.cloud;

import com.gluonhq.charm.connect.GluonClient;
import com.gluonhq.charm.connect.GluonClientBuilder;
import com.gluonhq.charm.connect.GluonCredentials;

/**
 * Creates a GluonClient based on valid credentials for this
 * application. It is the access point from Charm Connect to 
 * the Gluon Cloud service
 */
public class GluonClientProvider {

    /** To get a valid GluonClient it is required signing in here 
    * http://portal.gluonhq.com/rest/register
    * with a valid email address and a password.
    * 
    * Add the name of the application, and you will get the pair key/secret strings
    */
    private static final String APPKEY =   "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
    private static final String APPSECRET =   "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
    
    private static GluonClient gluonClient;

    public static GluonClient getGluonClient() {
        if (gluonClient == null) {
            gluonClient = GluonClientBuilder.create()
                    .credentials(new GluonCredentials(APPKEY, APPSECRET))
                    .build();
        }
        return gluonClient;
    }

}
