package com.gluonhq.demos.comments;

import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.demos.comments.views.comments.CommentsView;
import com.gluonhq.demos.comments.views.edit.EditView;
import com.gluonhq.demos.comments.views.home.HomePresenter;
import com.gluonhq.demos.comments.views.home.HomeView;
import javafx.scene.Scene;

/** CommentsFX is a sample app that uses Gluon Mobile http://gluonhq.com/products/mobile/
 * 
 * It also uses Afterburner framework https://github.com/AdamBien/afterburner.fx/
 * 
 * This is the main class of the project, extending MobileApplication, which also extends Application.
 * The developer just need to specify the views and layers in their application, 
 * and provide these as factories that can be called on-demand, starting from the 'home-view', 
 * which is shown when the application first starts.
 *
 * For information about Gluon Mobile licenses see http://gluonhq.com/products/mobile/buy/
 */
//@License(key="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX")
public class CommentsFX extends MobileApplication {
    
    public static final String COMMENTS_VIEW = "comments";
    public static final String EDIT_VIEW = "edit comment";
    
    private HomePresenter homePresenter;
    
    @Override
    public void init() throws Exception {
        super.init(); 
        
        /*
        Create default home view with list of comments
        */
        
        addViewFactory(HOME_VIEW,()->{
            HomeView homeView = new HomeView();
            View view = (View) homeView.getView();
            homePresenter = (HomePresenter)homeView.getPresenter();
            return view;
        });
        
        /*
        Create comments view
        */
        addViewFactory(COMMENTS_VIEW, ()->{
            CommentsView commentsView = new CommentsView();
            return (View)commentsView.getView();
        });
        
        /*
        Create edit comment view
        */
        addViewFactory(EDIT_VIEW, ()->{
            EditView editView = new EditView();
            return (View)editView.getView();
        });
    }
    
    /**
     * Called once we have a scene, allowing first run settings 
     * @param scene 
     */
    @Override
    public void postInit(Scene scene){
        Swatch.INDIGO.assignTo(scene);

        // once scene is ready, retrieve comments
        homePresenter.postInitialize();
    }
}

