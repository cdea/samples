package com.gluonhq.demos.comments.views.edit;

import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demos.comments.cloud.CommentsService;
import com.gluonhq.demos.comments.model.Comment;
import javafx.beans.binding.Bindings;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.inject.Inject;

/**
 * Controller for edit.fxml
 */
public class EditPresenter {

    @FXML 
    private View commentsView;
    
    @FXML
    private Avatar avatar;
    
    @FXML
    private TextField authorField;
    
    @FXML
    private TextArea commentsField;
    
    @FXML
    private Button apply;
    
    @Inject
    private CommentsService service;
    
    private Comment comment;
    
    public void initialize() {
        
        // apply enabled only if changes are made
        apply.disableProperty().bind(Bindings.createBooleanBinding(() -> {
            if(comment==null || comment.getAuthor() == null || comment.getContent() == null) {
                return true;
            }
            return authorField.textProperty().isEqualTo(comment.getAuthor())
                        .and(commentsField.textProperty().isEqualTo(comment.getContent())).get();
            }, authorField.textProperty(),commentsField.textProperty()));
        
        if (PlatformFactory.getPlatform().isTablet()) {
            avatar.getStyleClass().add("tablet");
        }
        // disable avatar in case comment is not editable
        PseudoClass pseudoClassDisable = PseudoClass.getPseudoClass("disabled");
                
        commentsView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                // retrieve comment from service
                comment = service.getEditComment();
                avatar.setImage(CommentsService.getUserImage(comment.getImageUrl()));
                authorField.setText(comment.getAuthor());
                authorField.setDisable(true);
                commentsField.setText(comment.getContent());
                // content is enabled if user is its author
                commentsField.setDisable(!comment.getNetworkId().equals(service.getUser().getNetworkId()));
                avatar.pseudoClassStateChanged(pseudoClassDisable, commentsField.isDisable());
                
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.ARROW_BACK.button(e -> onCancel()));
                appBar.setTitleText("Edit Comment");
            } else {
                authorField.setText("");
                commentsField.setText("");
            }
        });
    }

    @FXML
    private void onCancel(){
        MobileApplication.getInstance().goHome();
    }

    @FXML
    private void onApply() {
        comment.setAuthor(authorField.getText());
        comment.setContent(commentsField.getText());
        MobileApplication.getInstance().goHome();
    }

}
