package com.gluonhq.demos.comments.views.home;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Alert;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.layout.layer.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demos.comments.model.Comment;
import com.gluonhq.demos.comments.CommentsFX;
import com.gluonhq.demos.comments.cloud.CommentsService;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javax.inject.Inject;

/**
 * FXML Controller class for home.fxml
 *
 */
public class HomePresenter {

    @FXML 
    private View homeView;
    
    // a ListView that will contain the comments
    @FXML
    private ListView<Comment> comments;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    // different messages for placeholder in case there are no comments
    private static final String SIGN_IN_MESSAGE = "Please, sign in first\n"
                + "to gain online access\n"
                + "to Comments v2.0";
    
    private static final String NO_COMMENTS_MESSAGE = "There are no comments\n"
            + "Click on the Action button\n"
            + "to create one";
    
    private final BooleanProperty sliding = new SimpleBooleanProperty();
    
    private FloatingActionButton floatingActionButton;
    
    /**
     * Initializes the controller class.
     */
    public void initialize() {
        final Label label = new Label(SIGN_IN_MESSAGE);
        label.textProperty().bind(new When(service.connected())
                .then(NO_COMMENTS_MESSAGE)
                .otherwise(SIGN_IN_MESSAGE));
        comments.setPlaceholder(label);
        
        comments.setCellFactory(cell -> { 
            final CommentListCell commentListCell = new CommentListCell(
                    // userId
                    service.getUser().getNetworkId(),
                    // left button: delete comment, only author's comment can delete it
                    c -> {
                        if (service.getUser().getNetworkId().equals(c.getNetworkId())) {
                           showDialog(c);
                        }
                    },
                    // right button: edit comment, everybody can view it, only author can edit it
                    c -> {
                       service.setEditComment(c);
                       MobileApplication.getInstance().switchView(CommentsFX.EDIT_VIEW);
                    });
            
            // notify view that cell is sliding
           commentListCell.slidingProperty().addListener((obs,b,b1) -> sliding.set(b1));
           
           return commentListCell;   
        });
        
        comments.disableProperty().bind(service.connected().not());
        comments.itemsProperty().bind(service.commentsProperty());
        
        // Action button
        floatingActionButton = new FloatingActionButton();
        floatingActionButton.setOnAction(e -> {
            if (service.getUser() == null) {
                // if not registered yet, launch sign in view
                service.retrieveComments();
            } else {
                // enter comments view
                MobileApplication.getInstance().switchView(CommentsFX.COMMENTS_VIEW);
            }
        });
        updateFloatingButtonText();
        service.user().addListener(o -> updateFloatingButtonText());
        homeView.getLayers().add(floatingActionButton);
        
        // block scrolling when sliding
        homeView.addEventFilter(ScrollEvent.ANY, e -> {
            if (sliding.get() && e.getDeltaY() != 0) {
                e.consume();
            }
        });
        
        homeView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.COMMENT.button());
                appBar.setTitleText("The Gluon Comments App 2.0");
            }
        });
    }

    private void updateFloatingButtonText() {
        if (service.getUser() == null) {
            // if not registered yet, show icon of sign in view
            floatingActionButton.setText(MaterialDesignIcon.CLOUD_DOWNLOAD.text);
        } else {
            // show "+" icon, to enter comments view
            floatingActionButton.setText(MaterialDesignIcon.ADD.text);
        }
    }   
    
    
    /**
     * Called when scene is ready to retrieve comments
     */
    public void postInitialize() {
        service.retrieveComments();
    }
    
    /**
     * Create a Dialog for getting deletion confirmation
     * @param item 
     */
    private void showDialog(Comment item) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitleText("Confirm deletion");
        alert.setContentText("This comment will be deleted permanently.\nDo you want to continue?");
        
        Button yes = new Button("Yes, delete permanently");
        yes.setOnAction(e -> {
            alert.setResult(ButtonType.YES); 
            alert.hide();
        });
        yes.setDefaultButton(true);
        
        Button no = new Button("No");
        no.setCancelButton(true);
        no.setOnAction(e -> {
            alert.setResult(ButtonType.NO); 
            alert.hide();
        });
        alert.getButtons().setAll(yes,no);
        
        Optional result = alert.showAndWait();
        if(result.isPresent() && result.get().equals(ButtonType.YES)){
            /*
            With confirmation, delete the item from the ListView. This will be
            propagated to the Cloud, and from here to the rest of the clients
            */
            comments.getItems().remove(item);
        }    
    }
    
}
