package com.gluonhq.demos.comments.views.home;

import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.control.SlidingListTile;
import com.gluonhq.demos.comments.cloud.CommentsService;
import com.gluonhq.demos.comments.model.Comment;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.ListCell;

/**
 * Custom list cell for adding sliding options
 * It uses a ListTile control, with two graphic zones
 * wrapped in a SlidingListTile control
 */
public class CommentListCell extends ListCell<Comment> {

    private final SlidingListTile slidingTile;
    
    private final ListTile tile;
    private final Avatar avatar;
    
    private final String userId;
    private Comment currentItem;
    private ChangeListener<String> commentChangeListener;
    
    /**
     * Create a list cell without consumers for the buttons underneath the tile
     * @param userId the user id of the actual user
     */
    public CommentListCell(String userId){
        this(userId, null);
    }

    /**
     * Create a list cell without same consumers for the buttons underneath the tile
     * @param userId the user id of the actual user
     * @param consumer 
     */
    public CommentListCell(String userId, Consumer<Comment> consumer) {
        this(userId, consumer, consumer);
    }
    
    /**
     * Create a list cell with a consumer for each of the buttons underneath the tile
     * @param userId the user id of the actual user
     * @param consumerLeft
     * @param consumerRight 
     */
    public CommentListCell(String userId, Consumer<Comment> consumerLeft, Consumer<Comment> consumerRight) {
        
        this.userId = userId;
        
        // Create Tile and SlidingTile instances
        tile = new ListTile();
        avatar = new Avatar();
        if (PlatformFactory.getPlatform().isTablet()) {
            avatar.getStyleClass().add("tablet");
        }
        tile.setPrimaryGraphic(avatar);
        slidingTile = new SlidingListTile(tile);
        
        /*
        Add listeners to swipe left or right, based on given consumers
        */
        
        // edit mode, swipping from right to the left
        slidingTile.swipedLeftProperty().addListener((obs, ov, nv) -> {
            if (nv && consumerRight != null) {
                consumerRight.accept(currentItem);
            }
            slidingTile.resetTilePosition();
        });
        
        // delete mode, swipping from left to right
        slidingTile.swipedRightProperty().addListener((obs, ov, nv) -> {
            if (nv && consumerLeft != null) {
                consumerLeft.accept(currentItem);
            }
            slidingTile.resetTilePosition();
        });
        
        /*
        Listener to changes in comment (via edit in this app or from other clients)
        */
        commentChangeListener = (obs, ov, nv) -> {
            // make sure text updates are run on the JavaFX thread, because the change can come from
            // an sse event from gluon cloud when another application updates a comment
            Platform.runLater(() -> {
                if (currentItem != null) {
                    tile.textProperty().setAll(currentItem.getAuthor());
                    tile.textProperty().addAll(getContent(currentItem));
                } else {
                    tile.textProperty().clear();
                }
            });
        };
    }

    /**
     * Add a ListTile control to not empty cells
     * @param item the comment on the cell
     * @param empty 
     */
    @Override
    protected void updateItem(Comment item, boolean empty) {
        super.updateItem(item, empty);
        
        updateCurrentItem(item);

        if (!empty && item != null) {
            avatar.setImage(CommentsService.getUserImage(item.getImageUrl()));
            slidingTile.allowedProperty().set(userId.equals(item.getNetworkId()));
            tile.textProperty().setAll(item.getAuthor());
            tile.textProperty().addAll(getContent(item));
            setGraphic(slidingTile);
            setPadding(Insets.EMPTY);
        } else {
            setGraphic(null);
        }
    }
    
    /**
     * When comment changes, remove listener, set currentItem and add listener
     * @param comment 
     */
    private void updateCurrentItem(Comment comment) {
        if (currentItem == null || !currentItem.equals(comment)) {
            if (currentItem != null) {
                currentItem.authorProperty().removeListener(commentChangeListener);
                currentItem.contentProperty().removeListener(commentChangeListener);
            }

            currentItem = comment;

            if (currentItem != null) {
                currentItem.authorProperty().addListener(commentChangeListener);
                currentItem.contentProperty().addListener(commentChangeListener);
            }
        }
    }
    
    /**
     * format the content of the comment, adding ellipsis if more than two lines
     * @param comment
     * @return one or two lines 
     */
    private String[] getContent(Comment comment) {
        if (comment == null || comment.getContent() == null || comment.getContent().isEmpty()) {
            return new String[] {""};
        }
        
        final String[] lines = comment.getContent().split("\\n");
        if (lines.length > 2) {
            lines[1]=lines[1].concat(" ...");
        }
        return lines;
    }
    
    /**
     * Boolean property with sliding status of cell
     * @return 
     */
    public BooleanProperty slidingProperty() {
        return slidingTile.slidingProperty();
    }
}
