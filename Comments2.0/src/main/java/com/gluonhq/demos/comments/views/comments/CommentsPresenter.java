package com.gluonhq.demos.comments.views.comments;

import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demos.comments.model.Comment;
import com.gluonhq.demos.comments.cloud.CommentsService;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.inject.Inject;

/**
 * Controller for comments.fxml
 */
public class CommentsPresenter {

    @FXML 
    private View commentsView;
    
    @FXML
    private Avatar avatar;
    
    @FXML
    private TextField authorField;
    
    @FXML
    private TextArea commentsField;
    
    @FXML
    private Button submit;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    public void initialize() {
        
        // Enable submit only if both fields have content
        submit.disableProperty().bind(Bindings.createBooleanBinding(() -> {
                return authorField.textProperty().isEmpty()
                        .or(commentsField.textProperty().isEmpty()).get();
            }, authorField.textProperty(),commentsField.textProperty()));
        
        if (PlatformFactory.getPlatform().isTablet()) {
            avatar.getStyleClass().add("tablet");
        }
        avatar.setImage(CommentsService.getUserImage(service.getUser().getPicture()));
                
        commentsView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                authorField.setText(service.getUser().getName());
                authorField.setDisable(!authorField.getText().isEmpty());
                
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.ARROW_BACK.button(e -> onCancel()));
                appBar.setTitleText("Add Comment");
            } else {
                commentsField.setText("");
            }
        });
        
    }
    
    /**
     * Clear input fields and return to Home_View
     */
    @FXML
    private void onCancel(){
        MobileApplication.getInstance().goHome();
    }
    
    /**
     * Add comment to the remote list. It will be added to the ListView, and also
     * it will go to the cloud, and broadcasted to the rest of clients.
     * Return to Home View
     */
    @FXML
    private void onSubmit(){
        service.addComment(new Comment(authorField.getText(), commentsField.getText(), 
                                       service.getUser().getPicture(), service.getUser().getNetworkId()));
        MobileApplication.getInstance().goHome();
    }

}
