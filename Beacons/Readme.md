Beacons
=======

A JavaFX Application that shows how to use Bluetooth Low Energy service on your mobile project, both on Android and iOS.

It uses the new BLE service in Gluon Charm Down to access the native implementation in the mobile device.

Instructions
------------
To execute the sample, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`

Blog Post
---------

Read in more detail about this sample [here](http://gluonhq.com/scanning-bluetooth-low-energy-beacons-charm/)
