package com.gluonhq.beacons;

import com.gluonhq.beacons.view.HomeView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;

public class Beacons extends MobileApplication {

    public static final String UUID = "74278BDA-B644-4520-8F0C-720EAF059935";
    
    @Override
    public void init() {
        addViewFactory(HOME_VIEW, () -> (View) new HomeView().getView());
    }
    
    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE_GREY.assignTo(scene);
    }

}
