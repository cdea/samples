package com.gluonhq.beacons.view;

import com.gluonhq.beacons.Beacons;
import com.gluonhq.charm.down.common.BleService;
import com.gluonhq.charm.down.common.Platform;
import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.down.common.ble.Configuration;
import com.gluonhq.charm.down.common.ble.ScanDetection;
import com.gluonhq.charm.down.common.ble.ScanDetection.PROXIMITY;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import java.util.function.Consumer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class HomePresenter {

    @FXML 
    private View home;
    
    @FXML
    private Button buttonScan;

    @FXML
    private Button buttonStop;

    @FXML
    private Label labelUUID;

    @FXML
    private Label labelMajor;

    @FXML
    private Label labelMinor;

    @FXML
    private Label labelRSSI;

    @FXML
    private Label labelDistance;

    @FXML
    private Circle circleFar, circleNear, circleImmediate;
    
    private final ObjectProperty<ScanDetection> scanDetection = new SimpleObjectProperty<ScanDetection>() {
        @Override
        protected void invalidated() {
            labelUUID.setText(get().getUuid());
            labelMajor.setText(String.valueOf(get().getMajor()));
            labelMinor.setText(String.valueOf(get().getMinor()));
            labelRSSI.setText(String.valueOf(get().getRssi()));
            labelDistance.setText(get().getProximity().name());
        }
         
    };
    
    public void initialize() {
        Platform platform = PlatformFactory.getPlatform();
        BleService bleService = platform.getBleService();
        
        Consumer<ScanDetection> callback = (ScanDetection t) -> {
            javafx.application.Platform.runLater(() -> scanDetection.set(t));
        };
        
        circleFar.setFill(null);
        circleFar.setStroke(Color.TRANSPARENT);
        circleFar.strokeProperty().bind(Bindings.createObjectBinding(() -> {
            if (scanDetection.get() != null && scanDetection.get().getProximity().equals(PROXIMITY.FAR)) {
                circleFar.setEffect(new DropShadow(10, Color.GREEN));
                return Color.GREEN;
            }
            circleFar.setEffect(null);
            return Color.GRAY;
        }, labelDistance.textProperty()));
        circleNear.setFill(null);
        circleNear.setStroke(Color.TRANSPARENT);
        circleNear.strokeProperty().bind(Bindings.createObjectBinding(() -> {
            if (scanDetection.get() != null && scanDetection.get().getProximity().equals(PROXIMITY.NEAR)) {
                circleNear.setEffect(new DropShadow(15, Color.GREEN));
                return Color.GREEN;
            }
            circleNear.setEffect(null);
            return Color.GRAY;
        }, labelDistance.textProperty()));
        circleImmediate.setFill(null);
        circleImmediate.setStroke(Color.TRANSPARENT);
        circleImmediate.strokeProperty().bind(Bindings.createObjectBinding(() -> {
            if (scanDetection.get() != null && scanDetection.get().getProximity().equals(PROXIMITY.IMMEDIATE)) {
                circleImmediate.setEffect(new DropShadow(20, Color.GREEN));
                return Color.GREEN;
            }
            circleImmediate.setEffect(null);
            return Color.GRAY;
        }, labelDistance.textProperty()));
        
        buttonScan.setOnAction(e -> {
            bleService.stopScanning();
            Configuration conf = new Configuration(Beacons.UUID);
            bleService.startScanning(conf, callback);
            buttonStop.setDisable(false);
            buttonScan.setDisable(true);
        });
        buttonStop.setOnAction(e -> {
            bleService.stopScanning();
            buttonScan.setDisable(false);
            buttonStop.setDisable(true);
        });
        buttonStop.setDisable(true);
        
        home.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.BLUETOOTH_CONNECTED.button());
                appBar.setTitleText("The Beacons App");
            }
        });
    }
    
}
