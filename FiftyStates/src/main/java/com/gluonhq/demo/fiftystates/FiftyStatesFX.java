package com.gluonhq.demo.fiftystates;

import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.CharmListCell;
import com.gluonhq.charm.glisten.control.CharmListView;
import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.demo.fiftystates.model.Density;
import com.gluonhq.demo.fiftystates.model.Density.DENSITY;
import com.gluonhq.demo.fiftystates.model.USState;
import com.gluonhq.demo.fiftystates.model.USStates;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.util.StringConverter;

public class FiftyStatesFX extends MobileApplication {

    private FilteredList<USState> filteredList;
    
    private boolean ascending = true;
    
    @Override
    public void init() {

        /*
        Create default home view with a toolbar and a CharmListView instance
        */
        addViewFactory(HOME_VIEW, () -> {
            View homeView = new View(HOME_VIEW) {
                
                {
                    filteredList = new FilteredList<>(USStates.statesList, getStatePredicate(null));
                    
                    CharmListView<USState, DENSITY> charmListView = new CharmListView(filteredList);
                    setCenter(charmListView);
                    
                    charmListView.setPlaceholder(new Label("Nothing here yet"));
                    
                    /*
                    Standard Cells
                    */
                    charmListView.setComparator((s1, s2) -> Double.compare(s2.getDensity(), s1.getDensity()));
                    charmListView.setCellFactory(p -> new USStateCell());

                    /*
                    Header Cells
                    */
                    charmListView.setHeadersFunction(Density::getDensity);
                    charmListView.setHeaderComparator((d1, d2) -> d2.compareTo(d1));
                    charmListView.setConverter(new StringConverter<DENSITY>() {

                        @Override
                        public String toString(DENSITY d) {
                            return "From " + ((int) d.getIni()) + " up to " + ((int) d.getEnd()) + " pop/km" + "\u00B2";
                        }

                        @Override
                        public DENSITY fromString(String string) {
                            throw new UnsupportedOperationException("Not supported yet.");
                        }
                    });

                    charmListView.setHeaderCellFactory(p -> new CharmListCell<USState>() {

                        private final ListTile tile = new ListTile();
                        
                        {
                            Avatar avatar = new Avatar(16, USStates.getUSFlag());
                            tile.setPrimaryGraphic(avatar);
                            setText(null);
                        }
                        
                        @Override
                        public void updateItem(USState item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item != null && !empty) {
                                tile.textProperty().setAll("Density", charmListView.toString(item));
                                setGraphic(tile);
                            } else {
                                setGraphic(null);
                            }
                        }

                    }); 
                    
                    /* 
                    AppBar
                    */
                    showingProperty().addListener((obs, ov, nv) -> {
                        if (nv) {
                            final AppBar appBar = MobileApplication.getInstance().getAppBar();

                            Button sort = MaterialDesignIcon.SORT.button(e -> {
                                if (ascending) {
                                    charmListView.setHeaderComparator((d1, d2) -> d1.compareTo(d2));
                                    charmListView.setComparator((s1, s2) -> Double.compare(s1.getDensity(), s2.getDensity()));
                                    ascending = false;
                                } else {
                                    charmListView.setHeaderComparator((d1, d2) -> d2.compareTo(d1));
                                    charmListView.setComparator((s1, s2) -> Double.compare(s2.getDensity(), s1.getDensity()));
                                    ascending = true;
                                }
                            });
                            appBar.setNavIcon(MaterialDesignIcon.STAR.button());
                            appBar.setTitleText("50 States");
                            appBar.getActionItems().add(sort);
                            appBar.getMenuItems().setAll(buildFilterMenu());
                        }
                    });
                }
                
            };
            return homeView;
        });
        
    }
    
    /**
     * Called once we have a scene, allowing first run settings 
     * @param scene 
     */
    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE.assignTo(scene);
        scene.getStylesheets().add(getClass().getResource("root.css").toExternalForm());
    }
    
    private Predicate<USState> getStatePredicate(Double population) {
        return state -> population == null || state.getPopulation() >= population * 1_000_000;
    }
    
    private List<MenuItem> buildFilterMenu() {
        final List<MenuItem> menu = new ArrayList<>();

        EventHandler<ActionEvent> menuActionHandler = e -> {
            MenuItem item = (MenuItem) e.getSource();
            Double population = (Double) item.getUserData();
            filteredList.setPredicate(getStatePredicate(population));
        };
        
        ToggleGroup toggleGroup = new ToggleGroup();
        
        RadioMenuItem allStates = new RadioMenuItem("All States");
        allStates.setOnAction(menuActionHandler);
        allStates.setSelected(true);
        menu.add(allStates);
        toggleGroup.getToggles().add(allStates);
        
        List<Double> items = Arrays.asList(0.5, 1.0, 2.5, 5.0);
        for (Double d : items) {
            RadioMenuItem item = new RadioMenuItem("Population > " + d + "M");
            item.setUserData(d);
            item.setOnAction(menuActionHandler);
            menu.add(item);
            toggleGroup.getToggles().add(item);
        }
        
        return menu;
    }

}

