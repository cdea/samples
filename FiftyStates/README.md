
50 States
=========

 A JavaFX Application that uses new Gluon Charm custom control, CharmListView. 

 - Charm Glisten: Simple application framework, custom UI controls and customized existing JavaFX UI controls

Prerequisites
-------------

Requires Gluon Charm Developer Preview 3

Instructions
------------
To execute the sample, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`
* Desktop
> `./gradlew run`

Blog Post
---------

Read in more detail about this sample [here](http://gluonhq.com/charmlistview-new-control-in-town/)

