
Comments
========

 A JavaFX Application that uses Gluon Charm: 

 - Charm Connect: the client-side counterpart of the Gluon Cloud service 
 - Charm Glisten: Simple application framework, custom UI controls and customized existing JavaFX UI controls

Prerequisites
-------------

You need to register [here](http://gluonhq.com/products/cloud/register/) to get a valid account on Gluon Cloud and a pair of key/secret tokens to run the application.

Instructions
-----------------
To execute the sample, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`
* Desktop
> `./gradlew run`

Blog Post
---------

Read in more detail about this sample [here](http://gluonhq.com/comments-app-getting-started-with-gluon-charm/)