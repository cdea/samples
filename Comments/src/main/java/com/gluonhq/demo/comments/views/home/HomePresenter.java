package com.gluonhq.demo.comments.views.home;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.layout.layer.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.comments.model.Comment;
import com.gluonhq.demo.comments.CommentsFX;
import com.gluonhq.demo.comments.cloud.CommentsService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javax.inject.Inject;

/**
 * FXML Controller class for home.fxml
 *
 */
public class HomePresenter {

    @FXML 
    private View homeView;
    
    // a ListView that will contain the comments
    @FXML
    private ListView<Comment> comments;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    /**
     * Initializes the controller class.
     */
    public void initialize() {
        comments.setCellFactory(c -> new CommentListCell());

        service.retrieveComments();
        comments.itemsProperty().bind(service.commentsProperty());
        
        homeView.getLayers().add(new FloatingActionButton(MaterialDesignIcon.ADD.text, e -> {
            MobileApplication.getInstance().switchView(CommentsFX.COMMENTS_VIEW);
        }));
        
        homeView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.COMMENT.button());
                appBar.setTitleText("The Gluon Comments App");
            }
        });
    }    
    
}
