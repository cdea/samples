package com.gluonhq.demo.comments.views.comments;

import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.comments.model.Comment;
import com.gluonhq.demo.comments.cloud.CommentsService;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.inject.Inject;

public class CommentsPresenter {

    @FXML 
    private View commentsView;
    
    @FXML
    private TextField authorField;
    
    @FXML
    private TextArea commentsField;
    
    @FXML
    private Button submit;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    /**
     * Initializes the controller class.
     */
    public void initialize() {
        
        // Enable submit only if both fields have content
        submit.disableProperty().bind(Bindings.createBooleanBinding(() -> {
                return authorField.textProperty().isEmpty()
                        .or(commentsField.textProperty().isEmpty()).get();
            }, authorField.textProperty(), commentsField.textProperty()));
        
        // when this View is shown, move focus to the author field.
        commentsView.showingProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                commentsField.requestFocus();
                authorField.requestFocus();
                
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.ARROW_BACK.button(e -> onCancel()));
                appBar.setTitleText("Add Comment");
            }
        });
    }
    
    /**
     * Clear input fields and return to Home_View
     */
    @FXML
    private void onCancel(){
        authorField.setText("");
        commentsField.setText("");
        MobileApplication.getInstance().switchView(HOME_VIEW);
    }
    
    /**
     * Add comment to the remote list. It will be added to the ListView, and also
     * it will go to the cloud, and broadcasted to the rest of clients.
     * Return to Home View
     */
    @FXML
    private void onSubmit(){
        service.addComment(new Comment(authorField.getText(), commentsField.getText()));
        authorField.setText("");
        commentsField.setText("");
        MobileApplication.getInstance().switchView(HOME_VIEW);
    }

}
