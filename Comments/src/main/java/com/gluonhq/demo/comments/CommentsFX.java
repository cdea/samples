package com.gluonhq.demo.comments;

import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
// import com.gluonhq.charm.glisten.license.License;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.demo.comments.views.comments.CommentsView;
import com.gluonhq.demo.comments.views.home.HomeView;
import javafx.scene.Scene;

/** CommentsFX is a sample app that uses Gluon Mobile http://gluonhq.com/products/mobile/
 * 
 * It also uses Afterburner framework https://github.com/AdamBien/afterburner.fx/
 * 
 * This is the main class of the project, extending MobileApplication, which also extends Application.
 * The developer just need to specify the views and layers in their application, 
 * and provide these as factories that can be called on-demand, starting from the 'home-view', 
 * which is shown when the application first starts.
 * 
 * For information about Gluon Mobile licenses see http://gluonhq.com/products/mobile/buy/
 */

//@License(key="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX")
public class CommentsFX extends MobileApplication {
    
    public static final String COMMENTS_VIEW = "Comments View";

    @Override
    public void init() {
        
        /*
        Create default home view with a list of comments retrieved from the cloud, 
        and with deletion possibilities
        */
        
        addViewFactory(HOME_VIEW,() -> {
            HomeView homeView = new HomeView();
            return (View) homeView.getView();
        });
        
        /*
        Create comments view to add comments and store them on the cloud
        */
        addViewFactory(COMMENTS_VIEW, () -> {
            CommentsView commentsView = new CommentsView();
            return (View) commentsView.getView();
        });
    }
    
    /**
     * Called once we have a scene, allowing first run settings 
     * @param scene 
     */
    @Override
    public void postInit(Scene scene){
        Swatch.INDIGO.assignTo(scene);
    }

}
