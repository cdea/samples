package com.gluonhq.demo.comments.views.home;

import com.airhacks.afterburner.views.FXMLView;

/** 
 * Main View
 * Defined with home.fxml
 * See Afterburner.fx
 */
public class HomeView extends FXMLView {

}
